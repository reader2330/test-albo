# Test albo

Proyecto generado para la actualización y pago de regalias de los comic de dos personajes de Marvel

## Comenzando 🚀

Para poder ejectutar el programa haz los siguientes pasos.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Java  11
Maven
Mongo  4.0
```
## Consideraciones

Cambiar en el application.yaml

```
cron.batch: al tiempo actual para que se ejecute la sincronización - default 0 0 0 * * *

```

Se puede forzar la sincronización por el siguiente endpoint:

```
 GET - /batch/force
```
Documentación de pruebas

```
 Archivo index.html - Spring Rest Docs
```

```
 Documentación de Swagger para los endpoints
 
 /swagger-ui.html
```


```
 Habilitado test.albo.mx
 
 Habilitar_albo.png
```


### Instalación 🔧


_Primer paso_

```
Ejecutar assemble.sh
```

_Y Despues_

```
Ejecutar avengers.sh
```


## Construido con 🛠️



* [SpringBoot] - El framework de java solicitado
* [Java] - Lenguaje a usar
* [Maven] - Manejador de dependencias
* [Mongo] - Base de datos NoSQL




## Autores ✒️



* **Luis Fernando Aguirre Olvera** - *Desarrollo* - [reader2330](https://gitlab.com/reader2330)


## Expresiones de Gratitud 🎁

* Gracias por revisar mi prueba :)



---
