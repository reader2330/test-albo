package com.albo.marvel.api.client;

import com.albo.marvel.api.model.Character;
import com.albo.marvel.api.model.Comic;
import com.albo.marvel.api.model.ResponseMarvelApiGeneral;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

/***
 * Interface que sirve para poder hacer las peticiones al API de Marvel.
 * El uso de Feign nos da la facilidad de hacer peticiones declarativas
 * @author Luis Fernando Aguirre Olvera
 *
 */
@FeignClient(name = "marvel" , url = "${marvel.api.url}")
@Component
public interface MarvelApi {

    /***
     * Función para hacer la llamada al endpoint de personajes
     * @param params Map con los parametros a enviar a la api de marvel
     * @return ResponseEntity con datos recibidos por la API de marvel (personaje)
     */
    @GetMapping("characters")
    ResponseEntity<ResponseMarvelApiGeneral<Character>>getCharacterByName(@SpringQueryMap  Map<String, String> params);

    /***
     * Función para hacer la llamada al endpoint de los comics de un persona
     * @param id identificador del personaje
     * @param params Map con los parametros a enviar a la api de marvel
     * @return ResponseEntity con datos recibidos por la API de marvel (comic)
     */

    @GetMapping("characters/{characterId}/comics")
    ResponseEntity<ResponseMarvelApiGeneral<Comic>> getComicsByCharacter(@PathVariable("characterId") Integer id, @SpringQueryMap  Map<String, String> params);
}
