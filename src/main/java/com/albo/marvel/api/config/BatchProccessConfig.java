
package com.albo.marvel.api.config;

import com.albo.marvel.api.service.ColaboratorsDataServiceImpl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.Instant;

import java.util.concurrent.ExecutionException;


/***
 * Configuración para la automatización del proceso de syncronización de los datos
 * @author Luis Fernando Aguirre Olvera
 */
@Configuration
@EnableScheduling
@Slf4j
@RequiredArgsConstructor
public class BatchProccessConfig {


    private final ColaboratorsDataServiceImpl colaboratorsDataService;

    /***
     * Función para executar las tareas de actualización de datos
     * @throws ExecutionException Sucede cuando en la execución se genera un error
     * @throws InterruptedException Sucede cuando la execución es interrumpida
     */
    @Scheduled(cron = "${cron.batch}")
    public void executeRecover() throws ExecutionException, InterruptedException {
        Instant start = Instant.now();
        colaboratorsDataService.getDataColaboratorByName("Iron Man");
        colaboratorsDataService.getDataColaboratorByName("Captain America");
        Instant end = Instant.now();
        log.info("Actualización generada - "+ Duration.between(start, end));
    }
}
