package com.albo.marvel.api.config;

import com.albo.marvel.api.exception.CharacterNotFoundException;
import com.albo.marvel.api.exception.SyncException;
import com.albo.marvel.api.model.ResponseGeneral;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/***
 * @author Luis Fernando Aguirre Olvera
 * Manejador de excecpciones del proyecto, para poder retornar errores controlados y darle una mejor información al usuario.
 */
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    /***
     *
     * Manejador de las excepciones cuando un usuario manda un heroe que no existe
     * @param characterNotFoundException Excepcion que se generar al no encontrar al heror de la petición
     * @return Respuesta para el usuario , indicandole que no tenemos al heroe que esta solicitando
     */
    @ExceptionHandler(CharacterNotFoundException.class)
    public ResponseEntity<ResponseGeneral> handlerCharacterNotFound(CharacterNotFoundException characterNotFoundException){
        ResponseGeneral responseGeneral = new ResponseGeneral();
        responseGeneral.setCode(HttpStatus.NO_CONTENT.value());
        responseGeneral.setMsg(characterNotFoundException.getMessage());
        return  new ResponseEntity<>( responseGeneral, HttpStatus.NO_CONTENT);
    }

    /***
     * Manejador de las excepciones cuando la actualización de los datos falla
     * @param syncException Excepcion que se genera al existir un error al momento de la sincronizació
     * @return Respuesta para el cliente dando el mensaje de existe un error al generar la actualización
     */

    @ExceptionHandler(SyncException.class)
    public ResponseEntity<ResponseGeneral> handleSyncError(SyncException syncException){
        ResponseGeneral responseGeneral = new ResponseGeneral();
        responseGeneral.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        responseGeneral.setMsg(syncException.getMessage());
        return  new ResponseEntity<>( responseGeneral, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
