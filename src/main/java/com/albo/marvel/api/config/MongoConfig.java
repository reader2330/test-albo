package com.albo.marvel.api.config;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/***
 * Configuración para el manejo de la bd de mongo
 * mongo version 4.0
 */
@Configuration
@EnableMongoRepositories(basePackages = "com.albo.marvel.api.repository")
public class MongoConfig  {


    @Value("${mongo.url}")
    private String urlMongo;
    /**
     * Bean
     * Método para generar la conexión a la base de datos de mongo
     * @return MongoClient Cliente de mongo que tiene la conexión
     */
    @Bean
    public MongoClient mongo(){
        ConnectionString connectionString =  new ConnectionString(urlMongo);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder().applyConnectionString(connectionString).build();
        return MongoClients.create(mongoClientSettings);
    }

    /***
     * Bean
     * Método para generar el template de mongo , para generar diferentes operaciones relacionado con la base de datos.
     * @return MongoTemplae , objeto con referencia a la base de datos
     */
    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongo(), "marvel");
    }

}
