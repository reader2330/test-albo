package com.albo.marvel.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

/***
 * Configuración de swagger
 */
@Configuration
public class SwaggerConfig {

    /***
     * Bean
     * Creador del Docket para la configuración de swagger
     * @return Docket , objeto con las propiedades para configura a swagger
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(
                        RequestHandlerSelectors
                                .basePackage("com.albo.marvel.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    /***
     * Información del API
     * @return ApiInfo informacion del api a generar
     */
    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Test albo, Marvel Api",
                "Api para traer información de la bibloteca de albo, sobre los personajes de marvel",
                "API 1",
                "Terms of service",
                new Contact("Fernando Aguirre", "www.example.com", "read2330@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }

}
