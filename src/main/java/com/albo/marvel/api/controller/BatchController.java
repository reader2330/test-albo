package com.albo.marvel.api.controller;


import com.albo.marvel.api.exception.SyncException;
import com.albo.marvel.api.service.ColaboratorsDataServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("batch/")
@RequiredArgsConstructor
public class BatchController {
    private final ColaboratorsDataServiceImpl colaboratorsDataService;

    @ApiOperation(value = "Forzar la sincronización", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 500, message = "Error al generar la sincronización")
    })
    @GetMapping("force")
    public ResponseEntity<String> forceSync(){
        try {
            colaboratorsDataService.getDataColaboratorByName("Iron Man");
            colaboratorsDataService.getDataColaboratorByName("Captain America");
        } catch (ExecutionException | InterruptedException e) {
            throw new SyncException("Error al generar la sincronización");
        }
        return new ResponseEntity<> ("OK", HttpStatus.OK);
    }
}
