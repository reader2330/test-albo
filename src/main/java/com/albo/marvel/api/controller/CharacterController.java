package com.albo.marvel.api.controller;

import com.albo.marvel.api.model.CharacterResponse;
import com.albo.marvel.api.service.CharacterServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("marvel/")
@RequiredArgsConstructor
public class CharacterController {

    private final CharacterServiceImpl characterService;

    @ApiOperation(value = "Obtener los personajes con los que han participado y en que comics", response = CharacterResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 204, message = "Character not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping(value = "characters/{name}")
    public CharacterResponse getShareColaboratorCharacter(@ApiParam(value = "Nombre del personaje", required = true) @PathVariable  String name) {
       return  characterService.getCharacterByName(name);
    }
}
