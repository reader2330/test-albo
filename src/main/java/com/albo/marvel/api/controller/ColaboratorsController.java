package com.albo.marvel.api.controller;

import com.albo.marvel.api.model.CharacterResponse;
import com.albo.marvel.api.model.ColaboratorResponse;
import com.albo.marvel.api.service.ColaboratorsServiceImpl;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("marvel")
@RequiredArgsConstructor
@Slf4j
public class ColaboratorsController {
    private final ColaboratorsServiceImpl colaboratorsService;

    @ApiOperation(value = "Obtener los colaboradores de los comics en los que ha estado el personaje", response = ColaboratorResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 204, message = "Character not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping(value = "colaborators/{name}", produces = {"application/json"})
    public ColaboratorResponse getColaboratorsByName(@ApiParam(value = "Nombre del personaje", required = true) @PathVariable("name")  String name)  {
        return colaboratorsService.getColaboratorsByName(name);
    }

}
