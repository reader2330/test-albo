package com.albo.marvel.api.enums;


import lombok.Getter;

/***
 * Enum con todos los tipos de heroes solicitados
 */
@Getter
public enum NameAvengersEnum {
    IRONMAN("ironman", "Iron Man"),
    CAPITANAMERICA("capamerica", "Captian America");

    private final String nameInput;
    private final String nameHeroe;

    /***
     * Constructor de la enum
     * @param nameInput Nombre de petición
     * @param nameHeroe Nombre del heroe
     */
    NameAvengersEnum(String nameInput, String nameHeroe){
        this.nameHeroe = nameHeroe;
        this.nameInput = nameInput;
    }


}
