package com.albo.marvel.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/***
 * Excepción de personajes no encontrados
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class CharacterNotFoundException extends RuntimeException{

    /***
     * Constructor de la excepción
     * @param exception mensaje que llevara la excepción
     */
    public CharacterNotFoundException(String exception){
        super(exception);
    }
}
