package com.albo.marvel.api.exception;

/***
 * Excepción de error en la actualización
 */
public class SyncException extends RuntimeException{

    /***
     * Constructor de la excepción
     * @param msg mensaje que llevara la excepción
     */
    public SyncException(String msg){
        super(msg);
    }

}
