package com.albo.marvel.api.model;

import lombok.Data;

import java.util.Date;


@Data
public class Character {
    private Integer id;
    private String name;
    private String description;
    private Date modified;
    private Thumbnail thumbnail;
    private String resourceURI;
    private ComicList comics;
}
