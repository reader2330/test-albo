package com.albo.marvel.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class CharacterComic{
    @ApiModelProperty(notes = "Nombre del personaje")
    private String character;

    @ApiModelProperty(notes = "Comics en los que ha participado")
    private List<String> Comics;

    public CharacterComic(){
        this.Comics = new ArrayList<>();
    }
}
