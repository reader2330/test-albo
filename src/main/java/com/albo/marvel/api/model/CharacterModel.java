package com.albo.marvel.api.model;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class CharacterModel {
    private String last_sync;
    private Set<CharacterComic> characterComicList;
    private String name;

    public CharacterModel(){
        this.characterComicList = new HashSet<>();
    }
}
