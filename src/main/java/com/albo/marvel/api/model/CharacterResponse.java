package com.albo.marvel.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.util.Set;

@Data
public class CharacterResponse {
    @ApiModelProperty(notes = "Fecha de actualización")
    private String last_sync;
    @ApiModelProperty(notes = "Lista de personajes y sus comics")
    private Set<CharacterComic> characters;


}
