package com.albo.marvel.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class ColaboratorResponse {
    @ApiModelProperty(notes = "Fecha de actualización")
    private String last_sync;
    @ApiModelProperty(notes = "Nombre de los editores")
    private Set<String> editors;
    @ApiModelProperty(notes = "Nombre del los escritores")
    private Set<String> writers;
    @ApiModelProperty(notes = "Nombre del los coloristas")
    private Set<String> colorists;

    public ColaboratorResponse(){
        editors = new HashSet<>();
        writers = new HashSet<>();
        colorists = new HashSet<>();
    }

}
