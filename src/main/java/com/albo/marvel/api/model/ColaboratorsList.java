package com.albo.marvel.api.model;

import lombok.Data;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Data
public class ColaboratorsList {
    private Set<String> editors;
    private Set<String> writers;
    private Set<String> colorists;
    private List<CharacterComic> characterComicList;

    public ColaboratorsList(){
        editors  = new TreeSet<>();
        writers  = new TreeSet<>();
        colorists  = new TreeSet<>();
        characterComicList = new ArrayList<>();
    }

}
