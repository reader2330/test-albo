package com.albo.marvel.api.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ColaboratorsModel extends ColaboratorResponse {
    private String name;
}
