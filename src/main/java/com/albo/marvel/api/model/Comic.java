package com.albo.marvel.api.model;

import lombok.Data;

@Data
public class Comic {
    private Integer id;
    private String title;
    private CreatorList creators;
    private CreatorList characters;

}
