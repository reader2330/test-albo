package com.albo.marvel.api.model;

import lombok.Data;

import java.util.List;

@Data
public class ComicList {
    private Integer returned;
    private List<Items> items;
    private String collectionURI;
    private Integer available;
}
