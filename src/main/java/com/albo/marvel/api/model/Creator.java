package com.albo.marvel.api.model;

import lombok.Data;

@Data
public class Creator {
    private String name;
    private String role;
    private String resourceURI;
}
