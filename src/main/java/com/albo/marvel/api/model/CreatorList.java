package com.albo.marvel.api.model;

import lombok.Data;

import java.util.List;

@Data
public  class CreatorList {

    private Integer returned;
    private String collectionURI;
    private Integer available;
    private List<Creator> items;
}
