package com.albo.marvel.api.model;

import lombok.Data;

import java.util.List;

@Data
public class DataResponse<T> {
     private Integer offset;
     private Integer limit;
     private Integer total;
     private Integer count;
     private List<T> results;

}
