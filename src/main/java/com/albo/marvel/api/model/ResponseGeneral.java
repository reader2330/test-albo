package com.albo.marvel.api.model;

import lombok.Data;

@Data
public class ResponseGeneral {
    private String msg;
    private Integer code;
}
