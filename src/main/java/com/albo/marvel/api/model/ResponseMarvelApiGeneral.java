package com.albo.marvel.api.model;

import lombok.Data;

@Data
public class ResponseMarvelApiGeneral<T> {

    private DataResponse<T> data;
    private String etag;
    private String attributionHTML;
    private String attributionText;
    private String copyright;
    private String status;
    private Integer code;



}

