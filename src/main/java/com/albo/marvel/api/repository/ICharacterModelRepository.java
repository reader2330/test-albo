package com.albo.marvel.api.repository;

import com.albo.marvel.api.model.CharacterModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

/***
 * Repositorio de los datos de personaje y comics
 */
@Repository
public interface ICharacterModelRepository extends MongoRepository<CharacterModel, String> {

    /***
     * Función de busqueda por nombre
     * @param name Nombre del personaje
     * @return Stream de los resultados sobre la busqueda
     */
    @Query(value = "{name: ?0}", fields = "{name: 0}",sort = "{last_sync:-1}")
    Stream<CharacterModel> findName(String name);

    /***
     * Busqueda de por nombre y ver si tiene registros
     * @param name Nombre del personaje
     * @return Encontrar registro valido
     */
    default CharacterModel charactersByName(String name){
        return findName(name).findFirst().orElse(new CharacterModel());
    }

}
