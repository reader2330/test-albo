package com.albo.marvel.api.repository;


import com.albo.marvel.api.model.ColaboratorsModel;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

/***
 * Repositorio de los colaboradores
 */
@Repository
public interface IColaborateModelRepository extends MongoRepository<ColaboratorsModel, String> {

    /***
     * Función de busqueda por nombre
     * @param name Nombre del personaje
     * @return Stream de los resultados sobre la busqueda
     */
    @Query(value = "{name: ?0}", fields = "{name: 0}",sort = "{last_sync:-1}")
    Stream<ColaboratorsModel> findName(String name);

    /***
     * Busqueda de por nombre y ver si tiene registros
     * @param name Nombre del personaje
     * @return Encontrar registro valido
     */
    default ColaboratorsModel colaboratorsByName(String name){
        return findName(name).findFirst().orElse(new ColaboratorsModel());
    }
}
