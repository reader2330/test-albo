package com.albo.marvel.api.service;

import com.albo.marvel.api.exception.CharacterNotFoundException;
import com.albo.marvel.api.model.CharacterModel;
import com.albo.marvel.api.model.CharacterResponse;

import com.albo.marvel.api.repository.ICharacterModelRepository;
import com.albo.marvel.api.util.HeroUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/***
 * Servicio para el procesamiento de las peticiones sobre personajes y comics
 */
@Service
@RequiredArgsConstructor
public class CharacterServiceImpl implements ICharacterService{
    private final ICharacterModelRepository iCharacterModelRepository;

    /***
     * Función que obtiene a los personajes por el nombre
     * @param name Nombre del personaje a procesar
     * @return Respuesta con los datos solicitados
     * @throws CharacterNotFoundException cuando no encuentre al personaje solicitado
     */
    @Override
    public CharacterResponse getCharacterByName(String name) {
        CharacterResponse characterResponse =  new CharacterResponse();
        String nameHeroe = HeroUtil.getName(name);
        CharacterModel characterModel = iCharacterModelRepository.charactersByName(nameHeroe);
        if (characterModel != null){
            characterResponse.setLast_sync(characterModel.getLast_sync());
            characterResponse.setCharacters(characterModel.getCharacterComicList());
        } else {
            throw new CharacterNotFoundException("Character not found");
        }
        return characterResponse;


    }
}
