package com.albo.marvel.api.service;

import com.albo.marvel.api.model.CharacterComic;
import com.albo.marvel.api.model.CharacterModel;

import com.albo.marvel.api.repository.ICharacterModelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;

/***
 * Servicio para el manejor y agrupación de los datos obtenidos de la API de Marvel
 */
@Service
@RequiredArgsConstructor
public class CharactersDataServiceImpl implements ICharactersDataService{

    private final ICharacterModelRepository iCharacterModelRepository;

    /***
     * Función para procesar los datos y agrupación
     * @param characters Data de los personajes y comics
     */
    @Override
    public void processData(CharacterModel characters) {
        CharacterModel characterModel =  new CharacterModel();
         characters.getCharacterComicList()
                .stream()
                .filter(item -> !item.getComics().isEmpty())
                .collect(
                        Collectors.toMap(
                                CharacterComic::getCharacter,
                                d -> new CharacterComic(d.getCharacter(), d.getComics()),
                                (CharacterComic d1, CharacterComic d2) -> {
                                    d1.getComics().addAll(d2.getComics());
                                    return d1;
                                }
                        )).forEach((k,v) -> characterModel.getCharacterComicList().add(v));
        characterModel.setName(characters.getName());
        characterModel.setLast_sync(characters.getLast_sync());
        saveCharacterList(characterModel);

    }

    /***
     * Función para guardar la data de la sincronización
     * @param characterModel objecto a almacenar
     */
    public void saveCharacterList(CharacterModel characterModel) {
        iCharacterModelRepository.save(characterModel);
    }
}
