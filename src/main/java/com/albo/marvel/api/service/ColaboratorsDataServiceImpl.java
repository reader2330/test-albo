package com.albo.marvel.api.service;

import com.albo.marvel.api.client.MarvelApi;
import com.albo.marvel.api.exception.SyncException;
import com.albo.marvel.api.model.Character;
import com.albo.marvel.api.model.CharacterModel;
import com.albo.marvel.api.model.ColaboratorsList;
import com.albo.marvel.api.model.ColaboratorsModel;
import com.albo.marvel.api.model.DataResponse;
import com.albo.marvel.api.model.ResponseMarvelApiGeneral;
import com.albo.marvel.api.repository.IColaborateModelRepository;
import com.albo.marvel.api.util.RequestUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/***
 * Servicio para generar las peticiones de la actualización de los colaboradores y personajes
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ColaboratorsDataServiceImpl implements IColaboratorsDataService{

    private final MarvelApi marvelApi;
    private final ComicServiceImp comicServiceImp;
    private final RequestUtil requestUtil;
    private final IColaborateModelRepository iColaborateModelRepository;
    private final RecollectDataService recollectDataService;
    private final CharactersDataServiceImpl charactersDataService;

    /***
     * Función para ejecutar las peticiones y agrupar la información
     * @param name Nombre del peronsaje
     * @throws NullPointerException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Override
    public void getDataColaboratorByName(String name) throws NullPointerException, ExecutionException, InterruptedException {

        Map<String, String> params = requestUtil.generateCredentialsMarvel();
        params.put("name", name);
        ResponseEntity<ResponseMarvelApiGeneral<Character>> response = marvelApi.getCharacterByName(params);
        ColaboratorsModel colaboratorResponse = getDataCreators(Objects.requireNonNull(response.getBody()).getData());
        iColaborateModelRepository.save(colaboratorResponse);

    }

    /***
     * Funcion que genera las peticiones al API
     * @param total Numero total de comics
     * @param character Personaje relacinado
     * @return lista de tareas de forma asyncrona
     */
    public  List<CompletableFuture<ColaboratorsList>> generateTask(Integer total , Character character){
        List<CompletableFuture<ColaboratorsList>> futureList = new ArrayList<>();
        IntStream.range(0, total / 100 ).forEach(item ->   {
            try {
                futureList.add(recollectDataService.agrupateList(comicServiceImp.getComicByCharacter(character.getId(), Optional.of(100),Optional.of(item*100) ), character.getName()));
            } catch (InterruptedException | ExecutionException e) {
                log.info("Error ---> " + e);
                throw new SyncException("Error al generar la sincronización");

            }
        });
        return futureList;
    }

    /***
     * Agrupación de todas las tareas generadas
     * @param futureList lista de tareas
     * @return lista ya obteniendo los resultados de las tareas
     * @throws ExecutionException
     * @throws InterruptedException
     */

    public List<ColaboratorsList> transformFuture( List<CompletableFuture<ColaboratorsList>> futureList) throws ExecutionException, InterruptedException {
        CompletableFuture<Void> done = CompletableFuture.allOf(
                futureList.toArray(new CompletableFuture[futureList.size()])
        );
        return done.thenApply(v ->
                futureList
                        .stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.<ColaboratorsList>toList())
        ).get();
    }

    /***
     * Funcion para obtener a los colaboradores
     * @param dataResponse  objeto que tiene la inforamción de los creadores
     * @return Respuesta con el objeto a almacenar
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public ColaboratorsModel getDataCreators(DataResponse<Character> dataResponse) throws ExecutionException, InterruptedException {
        ColaboratorsModel colaboratorResponse =  new ColaboratorsModel();
        CharacterModel characterModel =  new CharacterModel();
        if (dataResponse.getCount() != 0){
            Character character = dataResponse.getResults().get(0);
            colaboratorResponse.setName(character.getName());

            List<ColaboratorsList> colaboratorsLists = transformFuture(generateTask(character.getComics().getAvailable(), character));

            colaboratorsLists.forEach(item -> {
                colaboratorResponse.getColorists().addAll(item.getColorists());
                colaboratorResponse.getEditors().addAll(item.getEditors());
                colaboratorResponse.getWriters().addAll(item.getWriters());
                characterModel.getCharacterComicList().addAll(item.getCharacterComicList());
            });

            characterModel.setName(character.getName());
            SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            colaboratorResponse.setLast_sync(simpleDateFormat.format(new Date()));
            characterModel.setLast_sync(simpleDateFormat.format(new Date()));
            charactersDataService.processData(characterModel);


        }
        return colaboratorResponse;
    }
}
