package com.albo.marvel.api.service;


import com.albo.marvel.api.exception.CharacterNotFoundException;
import com.albo.marvel.api.model.ColaboratorResponse;
import com.albo.marvel.api.model.ColaboratorsModel;
import com.albo.marvel.api.repository.IColaborateModelRepository;

import com.albo.marvel.api.util.HeroUtil;
import lombok.RequiredArgsConstructor;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;




/***
 * Servicio para el procesamiento de las peticiones sobre personajes y colaboradores
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ColaboratorsServiceImpl implements IColaboratorsService{




    private final IColaborateModelRepository iColaborateModelRepository;

    /***
     * Función que obtiene a los colaboradores por el nombre
     * @param name Nombre del personaje a procesar
     * @return Respuesta con los datos solicitados
     * @throws CharacterNotFoundException cuando no encuentre al personaje solicitado
     */
    @Override
    public ColaboratorResponse getColaboratorsByName(String name) {
        ColaboratorResponse colaboratorResponse =  new ColaboratorResponse();
        ColaboratorsModel colaboratorsModel = null;
        String nameHeroe = HeroUtil.getName(name);
        colaboratorsModel= iColaborateModelRepository.colaboratorsByName(nameHeroe);
        if (colaboratorsModel != null){
            colaboratorResponse.setLast_sync(colaboratorsModel.getLast_sync());
            colaboratorResponse.setWriters(colaboratorsModel.getWriters());
            colaboratorResponse.setColorists(colaboratorsModel.getColorists());
            colaboratorResponse.setEditors(colaboratorsModel.getEditors());
        }else {
            throw new CharacterNotFoundException("Charecter not found");
        }
        return colaboratorResponse;

    }
}
