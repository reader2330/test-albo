package com.albo.marvel.api.service;

import com.albo.marvel.api.client.MarvelApi;
import com.albo.marvel.api.model.Comic;
import com.albo.marvel.api.model.ResponseMarvelApiGeneral;
import com.albo.marvel.api.util.RequestUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import static java.util.concurrent.CompletableFuture.completedFuture;

/**
 *
 * Service para la consulta de comics
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ComicServiceImp  implements IComicService{

    /**
     * Client para la llamada a la api de marvel
     */
    private final MarvelApi api;
    /***
     * Component para la generación de credenciales
     */
    private final RequestUtil requestUtil;

    /***
     *
     * @param id Integer Id del personaje a buscar
     * @param limit Número limite de registros a traer
     * @param offset Número de registros ignorados
     */
    @Override
    @Async
    public CompletableFuture<ResponseMarvelApiGeneral<Comic>> getComicByCharacter(Integer id, Optional<Integer> limit, Optional<Integer> offset) {
        Map<String, String> request = requestUtil.generateCredentialsMarvel();
        limit.ifPresent(lim -> request.put("limit", lim.toString()));
        offset.ifPresent(off -> request.put("offset", off.toString()));
        return  CompletableFuture.completedFuture(api.getComicsByCharacter(id, request).getBody());
    }
}
