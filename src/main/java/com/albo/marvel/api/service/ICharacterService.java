package com.albo.marvel.api.service;

import com.albo.marvel.api.exception.CharacterNotFoundException;
import com.albo.marvel.api.model.CharacterResponse;


/***
 * Servicio para el procesamiento de las peticiones sobre personajes y comics
 */
public interface ICharacterService {

    /***
     * Función que obtiene a los personajes por el nombre
     * @param name Nombre del personaje a procesar
     * @return Respuesta con los datos solicitados
     * @throws CharacterNotFoundException cuando no encuentre al personaje solicitado
     */
    CharacterResponse getCharacterByName(String name);
}
