package com.albo.marvel.api.service;

import com.albo.marvel.api.model.CharacterModel;
import com.albo.marvel.api.model.ColaboratorResponse;

import java.util.concurrent.ExecutionException;


/***
 * Servicio para el manejor y agrupación de los datos obtenidos de la API de Marvel
 */
public interface ICharactersDataService {
    /***
     * Función para procesar los datos y agrupación
     * @param characters Data de los personajes y comics
     */
    void processData(CharacterModel characters);
}
