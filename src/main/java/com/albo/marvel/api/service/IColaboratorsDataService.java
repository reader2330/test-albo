package com.albo.marvel.api.service;



import java.util.concurrent.ExecutionException;

/***
 * Servicio para generar las peticiones de la actualización de los colaboradores y personajes
 */
public interface IColaboratorsDataService {

    /***
     * Función para ejecutar las peticiones y agrupar la información
     * @param name Nombre del peronsaje
     * @throws NullPointerException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    void getDataColaboratorByName(String name) throws InterruptedException, ExecutionException;
}
