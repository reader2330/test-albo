package com.albo.marvel.api.service;


import com.albo.marvel.api.exception.CharacterNotFoundException;
import com.albo.marvel.api.model.ColaboratorResponse;


/***
 * Servicio para el procesamiento de las peticiones sobre personajes y colaboradores
 */
public interface IColaboratorsService {

    /***
     * Función que obtiene a los colaboradores por el nombre
     * @param name Nombre del personaje a procesar
     * @return Respuesta con los datos solicitados
     * @throws CharacterNotFoundException cuando no encuentre al personaje solicitado
     */
    ColaboratorResponse getColaboratorsByName(String name);
}
