package com.albo.marvel.api.service;

import com.albo.marvel.api.model.Comic;
import com.albo.marvel.api.model.ResponseMarvelApiGeneral;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 *
 * Service para la consulta de comics
 */
public interface IComicService {


    /***
     *
     * @param id Integer Id del personaje a buscar
     * @param limit Número limite de registros a traer
     * @param offset Número de registros ignorados
     */
    CompletableFuture<ResponseMarvelApiGeneral<Comic>> getComicByCharacter(Integer id, Optional<Integer> limit, Optional<Integer> offset);
}
