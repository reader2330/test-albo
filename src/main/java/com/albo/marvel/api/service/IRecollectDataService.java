package com.albo.marvel.api.service;

import com.albo.marvel.api.model.ColaboratorsList;
import com.albo.marvel.api.model.Comic;
import com.albo.marvel.api.model.ResponseMarvelApiGeneral;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/***
 * Servicio de para la recolección y preparación de información
 */
public interface IRecollectDataService {

    /***
     * Método asyncrono de agrupación de información
     * @param task Tarea que obtiene la información de la API
     * @param nameCharacter nombre del personaje
     * @return Future con la respuesta generada despues de la manipulación
     * @throws InterruptedException
     * @throws ExecutionException
     */
    CompletableFuture<ColaboratorsList> agrupateList(CompletableFuture<ResponseMarvelApiGeneral<Comic>> task, String name) throws ExecutionException, InterruptedException;
}
