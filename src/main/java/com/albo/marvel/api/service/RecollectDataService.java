package com.albo.marvel.api.service;

import com.albo.marvel.api.model.CharacterComic;
import com.albo.marvel.api.model.ColaboratorsList;
import com.albo.marvel.api.model.Comic;
import com.albo.marvel.api.model.Creator;
import com.albo.marvel.api.model.DataResponse;
import com.albo.marvel.api.model.ResponseMarvelApiGeneral;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/***
 * Servicio de para la recolección y preparación de información
 */
@Service
@Slf4j
public class RecollectDataService implements IRecollectDataService {

    ColaboratorsList colaboratorsList = new ColaboratorsList();

    @Value("#{'${marvel.api.filters}'.split(',')}")
    public List<String> filters;

    /***
     * Método asyncrono de agrupación de información
     * @param task Tarea que obtiene la información de la API
     * @param nameCharacter nombre del personaje
     * @return Future con la respuesta generada despues de la manipulación
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Override
    @Async
    public CompletableFuture<ColaboratorsList> agrupateList(CompletableFuture<ResponseMarvelApiGeneral<Comic>> task, String nameCharacter) throws InterruptedException, ExecutionException {
        DataResponse<Comic> dataResponse = task.get().getData();

        dataResponse.getResults().forEach(item -> this.setCharacters(item, nameCharacter));
        dataResponse.getResults()
                .stream()
                .map(item -> item.getCreators().getItems())
                .flatMap(List::stream)
                .filter(this::checkTypeCreators)
                .forEach(this::setCreatorList);

        return CompletableFuture.completedFuture(colaboratorsList);
    }

    public  boolean checkTypeCreators(Creator creator){
        return filters.stream().anyMatch(i -> i.equals(creator.getRole()));
    }

    /***
     * Función para la manipulación de la informacio de los personajes
     * @param comic Comic relacionado
     * @param nameCharacterActual Personaje de referencia
     */
    public void setCharacters(Comic comic, String nameCharacterActual){

        comic
                .getCharacters()
                .getItems()
                .stream()
                .filter(item -> !item.getName().equals(nameCharacterActual))
                .forEach(character -> {
                    CharacterComic characterComic =  new CharacterComic();
                    characterComic.setCharacter(character.getName());
                    characterComic.getComics().add(comic.getTitle());
                    colaboratorsList.getCharacterComicList().add(characterComic);
                });
    }

    /***
     * Función para identificar y agregar al tipo de creador o colaborador
     * @param creator Creador a verificar y agregar
     */
    public void setCreatorList(Creator creator){
        String role =  creator.getRole();
        if (role.equals(filters.get(0))){
            colaboratorsList.getEditors().add(creator.getName());
        }
        if (role.equals(filters.get(1))){
            colaboratorsList.getWriters().add(creator.getName());
        }
        if (role.equals(filters.get(2))){
            colaboratorsList.getColorists().add(creator.getName());
        }

    }

}
