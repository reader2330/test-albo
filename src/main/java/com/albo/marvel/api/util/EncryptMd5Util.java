package com.albo.marvel.api.util;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import org.springframework.util.DigestUtils;

import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/***
 * Utileria para la encryptación de las llaves
 */
public final class EncryptMd5Util {

    /***
     * Constructor privado
     */
    private EncryptMd5Util(){}

    /***
     * Método para generar el hash
     * @param word
     * @return Hash para agregar a la petición
     */
    public static String encryptToMd5(String word ){
        return  DigestUtils.md5DigestAsHex(word.getBytes(StandardCharsets.UTF_8));
    }
}
