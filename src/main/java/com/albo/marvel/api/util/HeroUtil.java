package com.albo.marvel.api.util;

import com.albo.marvel.api.enums.NameAvengersEnum;
import com.albo.marvel.api.exception.CharacterNotFoundException;

import java.util.Arrays;

/***
 * Utileria para la verificación de personaje valido
 */
public final class HeroUtil {

    /***
     * Obtencion del nombre del personae
     * @param name  nombre de entrada
     * @return nombre del personaje
     */
    public static String getName(String name){
        return Arrays.stream(NameAvengersEnum.values())
                .filter(e -> e.getNameInput().equals(name))
                .findFirst()
                .orElseThrow(() -> new CharacterNotFoundException("Character not found")).getNameHeroe();
    }

}
