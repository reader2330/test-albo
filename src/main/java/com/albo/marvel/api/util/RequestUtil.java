package com.albo.marvel.api.util;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/***
 * Componente para generacion de la peticiones
 */
@Slf4j
@Component
public final class RequestUtil {


    @Value("${marvel.api.publicKey}")
    private  String publicKey;
    @Value("${marvel.api.privateKey}")
    private  String privateKey;

    /***
     * Constructor privado
     */
    private RequestUtil(){ }

    /***
     * Función generadora de las peticiones iniciales
     * @return Map con valores para la petición
     */
    public  Map<String, String> generateCredentialsMarvel(){
        Map<String, String> params =  new HashMap<>();
        Long ts = new Date().getTime();
        String hash = EncryptMd5Util.encryptToMd5(ts + privateKey + publicKey);
        params.put("ts", ts.toString());
        params.put("apikey", publicKey);
        params.put("hash", hash);
        return params;
    }

}
