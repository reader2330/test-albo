package com.albo.marvel.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class CharacterControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void noFoundCharacter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/marvel/characters/iron"))
                .andExpect(status().isNoContent())
                .andExpect(content().json("{code: 204, msg: 'Character not found'}"))
                .andDo(document(
                        "character-not-found-json", responseFields(
                                fieldWithPath("msg").description("Mensaje retornado"),
                                fieldWithPath("code").description("Codigo de error")
                        )));

    }


    @Test
    public void foundCharacter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/marvel/characters/ironman"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("last_sync").exists())
                .andDo(document(
                        "character-found-json", responseFields(
                                fieldWithPath("last_sync").description("Fecha de sincronización"),
                                fieldWithPath("characters[].character").description("Nombre del personaje"),
                                fieldWithPath("characters[].comics").description("Comics donde aparece")
                        )));

    }
}
