package com.albo.marvel.api.controller;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class ColaboratorControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void noFoundCharacter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/marvel/colaborators/iron"))
                .andExpect(status().isNoContent())
                .andExpect(content().json("{code: 204, msg: 'Character not found'}"))
                .andDo(document(
                        "colaborator-not-found-json", responseFields(
                                fieldWithPath("msg").description("Mensaje retornado"),
                                fieldWithPath("code").description("Codigo de error")
                        )));

    }


    @Test
    public void foundCharacter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/marvel/colaborators/ironman"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("last_sync").exists())
                .andDo(document(
                        "colaborator-found-json", responseFields(
                                fieldWithPath("last_sync").description("Fecha de sincronización"),
                                fieldWithPath("editors[]").description("Nombre de los editores"),
                                fieldWithPath("writers[]").description("Nombre de los escritores"),
                                fieldWithPath("colorists[]").description("Nombre de los coloristas")
                        )));

    }
}
